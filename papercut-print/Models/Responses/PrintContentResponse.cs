﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace papercut_print.Models.Responses
{
    public class PrintContentResponse
    {
        public PrintContentResponse()
        {
            PrintContents = new List<PrintContentModel>();
        }
        public List<PrintContentModel> PrintContents { get; set; }
        public int TotalCost { get; set; }
        public string TotalCostStr => (TotalCost / 100.0).ToString("C");
    }
}