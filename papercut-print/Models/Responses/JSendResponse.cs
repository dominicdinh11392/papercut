﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace papercut_print.Models.Responses
{
    public class JSendResponse
    {
        public string Key { get; set; } // key
        public string Status { get; set; } // success or failed
        public object Data { get; set; } // data and message object
        public object Ext { get; set; } // for paging

        public void DoKey(string key)
        {
            Key = key;
        }

        public void DoSuccess()
        {
            Status = "success";
        }

        public void DoFailed()
        {
            Status = "failed";
        }

        public void DoData(object data)
        {
            Data = data;
        }

        public void DoMessage(string msg)
        {
            Data = new JMessage(msg);
        }

        public void DoExt(int pageNumber, int pageSize, int totalCount)
        {
            Ext = new JPaging(pageNumber, pageSize, totalCount); ;
        }
    }
    public class JMessage
    {
        public string Msg { get; set; }

        public JMessage(string msg)
        {
            Msg = msg;
        }
    }

    public class JPaging
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }

        public JPaging(int pageNumber, int pageSize, int totalCount)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            TotalCount = totalCount;
        }
    }
}