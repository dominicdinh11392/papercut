﻿using System.Collections.Generic;

namespace papercut_print.Models
{
    public class PrintContentModel
    {
        public PrintContentModel()
        {
            Validation = new List<string>();
        }
        public int TotalPage { get; set; }
        public string TotalPageStr { get; set; }

        public int ColorPage { get; set; }
        public string ColorPageStr { get; set; }

        public int TotalCost { get; set; }
        public string TotalCostStr => (TotalCost / 100.0).ToString("C");


        public bool IsDoubleSided { get; set; }
        public string IsDoubleSidedStr { get; set; }
        public string IsDoubleSidedStrChange => IsDoubleSided ? "Yes" : "No";

        public List<string> Validation { get; set; }
    }
}