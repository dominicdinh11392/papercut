﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using papercut_print.Common;
using papercut_print.Models;
using papercut_print.Models.Responses;
using static papercut_print.Common.AppErrorCode;
using static papercut_print.Common.AppErrorMessages;
using static papercut_print.Utils.AppHelpers;

namespace papercut_print.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// upload csv file handling
        /// </summary>
        /// <param name="postedFile">csv file</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UploadFile()
        {
            var result = new JSendResponse();
            #region [query]

            #endregion [query]

            #region [validate]
            if (Request.Files == null)
            {
                result.DoFailed();
                result.DoMessage(Messages[FileRequired]);
                return Json(new { result });
            }
            if (Request.Files.Count == 0)
            {
                result.DoFailed();
                result.DoMessage(Messages[FileRequired]);
                return Json(new { result });
            }

            var postedFile = Request.Files[0];

            if (postedFile == null)
            {
                result.DoFailed();
                result.DoMessage(Messages[FileRequired]);
                return Json(new { result });
            }

            if (postedFile.ContentLength == 0) // postedFile content length 
            {
                result.DoFailed();
                result.DoMessage(Messages[FileEmpty]);
                return Json(new { result });
            }

            if (postedFile.ContentLength > Constants.MaximumFileSize * 1024 * 1024) // postedFile large 
            {
                result.DoFailed();
                result.DoMessage(Messages[FileLarge]);
                return Json(new { result });
            }

            var extension = Path.GetExtension(postedFile.FileName);
            if (string.IsNullOrEmpty(extension) || extension.ToLower() != Constants.CsvFile) // postedFile type
            {
                result.DoFailed();
                result.DoMessage(Messages[FileFormatInvalid]);
                return Json(new { result });
            }

            #endregion  [validate]

            #region [main]

            // reading file & calculate
            var response = new PrintContentResponse();
            try
            {
                using (var sReader = new StreamReader(postedFile.InputStream))
                {
                    var row = 0;
                    //Loop through the records
                    while (!sReader.EndOfStream)
                    {
                        row++;
                        if (row == 1) continue;

                        var rowData = sReader.ReadToEnd().Split('\n');

                        for (var i = 1; i < rowData.Count(x => !string.IsNullOrEmpty(x)); i++)
                        {
                            var dataConverted = ConvertData(rowData[i]);

                            response.PrintContents.Add(dataConverted);
                            
                            if (dataConverted.IsDoubleSided)
                            {
                                dataConverted.TotalCost += dataConverted.ColorPage * Constants.DoubleColorPagePrice
                                                           + (dataConverted.TotalPage - dataConverted.ColorPage) * Constants.DoubleBackWhitePagePrice;
                            }
                            else
                            {
                                dataConverted.TotalCost += dataConverted.ColorPage * Constants.SingleColorPagePrice
                                                           + (dataConverted.TotalPage - dataConverted.ColorPage) * Constants.SingleBackWhitePagePrice;
                            }

                            response.TotalCost += dataConverted.TotalCost;
                        }

                    }
                }

                result.DoSuccess();
                result.DoData(response);
                return Json(new { result });
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex.StackTrace}, {ex.Message}");
                result.DoFailed();
                result.DoMessage(Messages[BadRequest]);
                return Json(new
                {
                    result
                });
            }
            #endregion  [main]
        }

        public ActionResult LoadPrintResult(PrintContentResponse model)
        {
            return PartialView("Patials/_PrintContentResult", model);
        }

        #region [private functions helper]

        public PrintContentModel ConvertData<T>(T rowData)
        {
            var data = ConvertValueToString(rowData).Split(',');
            var result = new PrintContentModel();
            try
            {
                result.TotalPageStr = ConvertValueToString(data[0]);
                result.ColorPageStr = ConvertValueToString(data[1]);
                result.IsDoubleSidedStr = ConvertValueToString(data[2]);

                result.TotalPage = int.Parse(result.TotalPageStr);
                result.ColorPage = int.Parse(result.ColorPageStr);
                result.IsDoubleSided = ConvertValueToBool(result.IsDoubleSidedStr);

                if (result.TotalPage < result.ColorPage)
                {
                    result.Validation.Add(Messages[WrongLogic]);
                }
            }
            catch (Exception)
            {
                result.Validation.Add(Messages[InvalidFormat]);
            }
            return result;
        }

        #endregion [private functions helper]
    }
}