﻿using System.Collections.Generic;

namespace papercut_print.Common
{
    public static class Constants
    {
        public const int MaximumFileSize = 4;
        public const string CsvFile = ".csv";
        public const int SingleBackWhitePagePrice = 15;
        public const int SingleColorPagePrice = 25;
        public const int DoubleBackWhitePagePrice = 10;
        public const int DoubleColorPagePrice = 20;
    }

    public static class AppErrorCode
    {
        #region [required]

        public const string FileRequired = "FileRequired";
        public const string FileEmpty = "FileEmpty";

        #endregion [required]

        #region [invalid]

        public const string FileFormatInvalid = "FileFormatInvalid";
        public const string InvalidFormat = "InvalidFormat";
        public const string FileLarge = "FileLarge";

        #endregion [invalid]

        #region [others]

        public const string InternalServerError = "InternalServerError";
        public const string BadRequest = "BadRequest";
        public const string WrongLogic = "WrongLogic";

        #endregion [others]
    }

    public static class AppErrorMessages
    {
        public static Dictionary<string, string> Messages = new Dictionary<string, string>()
        {
            #region [required]

            {
                AppErrorCode.FileRequired, "Please select the file first to upload."
            },
            {
                AppErrorCode.FileEmpty, "Please select a file that contains content."
            },

            #endregion

            #region [invalid]

            {
                AppErrorCode.FileFormatInvalid, "Please select the csv file with .csv extension."
            },  
            {
                AppErrorCode.InvalidFormat, "Invalid format."
            },
            {
                AppErrorCode.FileLarge, "Please select files smaller than 4M."
            },

            #endregion [invalid]

            #region [others]

            {
                AppErrorCode.InternalServerError, "Something went wrong. Please try again."
            },
            {
                AppErrorCode.BadRequest, "Something went wrong. Please try again."
            },
            {
                AppErrorCode.WrongLogic,  "Total pages can not less than Color pages."
            }

            #endregion
        };
    }
}