﻿using System;

namespace papercut_print.Utils
{
    #region [convert]

    public static class AppHelpers
    {
        /// <summary>
        /// convert any data type to string
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ConvertValueToString<T>(T value)
        {
            return value.ToString();
        }

        /// <summary>
        /// convert any data type to integer
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int ConvertValueToInt<T>(T value)
        {
            try
            {
                return Convert.ToInt32(value);
            }
            catch
            {
                return -1;
            }
        }

        /// <summary>
        /// convert any data type to bool
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool ConvertValueToBool<T>(T value)
        {
            try
            {
                return Convert.ToBoolean(value);
            }
            catch
            {
                return false;
            }
        }

        #endregion  [convert]

    }
}