﻿// [js for upload section]
function readURL(input) {
    if (input.files && input.files[0]) {

        var reader = new FileReader();

        reader.onload = function (e) {
            $(".image-upload-wrap").hide();

            $(".file-upload-image").attr("src", e.target.result);
            $(".file-upload-content").show();

            $(".image-title").html(input.files[0].name);
        };

        reader.readAsDataURL(input.files[0]);

    } else {
        removeUpload();
    }
}

function removeUpload() {
    $(".file-upload-input").replaceWith($(".file-upload-input").clone());
    $(".file-upload-content").hide();
    $(".image-upload-wrap").show();
}
$(".image-upload-wrap").bind("dragover", function () {
    $(".image-upload-wrap").addClass("image-dropping");
});
$(".image-upload-wrap").bind("dragleave", function () {
    $(".image-upload-wrap").removeClass("image-dropping");
});
// [js for upload section]

// [upload and calculate cost]
function UploadFile() {
    $("#errorMsg").text("");
    $("#calculate-result-content").html("");
    var formData = new FormData($("#upload_form")[0]);
    formData.append("file", $("input[type=file]")[0].files[0]);
    $.ajax({
        type: "POST",
        url: "/Home/UploadFile",
        data: formData,
        contentType: false,
        processData: false,
        success: function (response) {
            var result = response.result;
            if (result.Status === "success") {
                $("#calculate-result-content").load("/Home/LoadPrintResult", { model: result.Data });
            } else {
                $("#errorMsg").text(result.Data.Msg);
            }

            removeUpload(); // clear file selected
        }
    });
};
